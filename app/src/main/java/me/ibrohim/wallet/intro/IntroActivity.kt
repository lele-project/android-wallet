package me.ibrohim.wallet.intro

import android.content.Intent
import android.os.Bundle
import com.github.paolorotolo.appintro.AppIntroFragment
import android.support.annotation.Nullable
import android.support.v4.app.Fragment
import android.support.v7.widget.AppCompatEditText
import android.widget.TextView
import com.github.paolorotolo.appintro.AppIntro
import me.ibrohim.wallet.R
import me.ibrohim.wallet.auth.AuthActivity
import me.ibrohim.wallet.auth.PassphraseService
import me.ibrohim.wallet.auth.SetAuthActivity

class IntroActivity : AppIntro() {

    private val passphraseSlide = Slide.newInstance(R.layout.intro_passphrase)

    override fun onCreate(@Nullable savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        addSlide(AppIntroFragment.newInstance("Hi!", "Welcome to the future", R.drawable.logo, resources.getColor(R.color.colorPrimary)))
        addSlide(AppIntroFragment.newInstance("Secure Wallet", "Your passphrase will be encrypted in your phone", R.drawable.ic_secure, resources.getColor(R.color.colorAccent)))
        addSlide(passphraseSlide)

        showSkipButton(false)
    }

    override fun onSkipPressed(currentFragment: Fragment) {
        super.onSkipPressed(currentFragment)
        finish()
    }

    override fun onDonePressed(currentFragment: Fragment) {
        val edittextPassphrase = passphraseSlide.view!!.findViewById<AppCompatEditText>(R.id.edittext_passphrase)

        if (!edittextPassphrase.text.isBlank()) {

            super.onDonePressed(currentFragment)

            val authIntent = Intent(this, SetAuthActivity::class.java)
                    .setAction(Intent.ACTION_EDIT)
                    .putExtra(PassphraseService.PASSPHRASE_KEY, edittextPassphrase.text.toString())

            this.startActivity(authIntent)
            finish()

        } else {

            edittextPassphrase.error = "Passphrase must not be empty"

        }
    }
}