package me.ibrohim.wallet.adapter

import android.annotation.SuppressLint
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import me.ibrohim.wallet.R

/**
 * @author ibrohimislam
 */

class TransactionDetailAdapter(private var values: List<Pair<String, String>>) : RecyclerView.Adapter<TransactionDetailAdapter.ViewHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemView = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_transaction_detail, parent, false)

        return ViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: ViewHolder?, position: Int) {
        val property = values[position]
        holder?.setProperty(property.first, property.second)
    }

    override fun getItemCount(): Int {
        return values.count()
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        private val detailField: TextView = view.findViewById(R.id.detail_field)
        private val detailValue: TextView = view.findViewById(R.id.detail_value)

        @SuppressLint("SetTextI18n")
        fun setProperty(field: String, value: String) {
            detailField.text = field
            detailValue.text = value

            if (detailValue.text.length > 40) {
                detailValue.text = "${detailValue.text.subSequence(0, 30)}..."
            }

        }
    }
}
