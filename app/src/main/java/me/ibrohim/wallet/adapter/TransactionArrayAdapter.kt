package me.ibrohim.wallet.adapter

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.AppCompatImageView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView

import me.ibrohim.wallet.R
import me.ibrohim.wallet.network.nxt.Transaction
import android.support.v7.widget.RecyclerView
import me.ibrohim.wallet.TransactionDetailActivity
import me.ibrohim.wallet.utils.DisplayBalance

/**
 * @author ibrohimislam
 */

class TransactionArrayAdapter(private val activity: AppCompatActivity, private val currentAccountRS: String, var values: Array<Transaction>) : RecyclerView.Adapter<TransactionArrayAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemView = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_transaction, parent, false)

        return ViewHolder(activity, currentAccountRS, itemView)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val transaction = values[position]
        holder.setTransaction(transaction)
    }

    override fun getItemCount(): Int {
        return values.count()
    }

    class ViewHolder(private val activity: AppCompatActivity, private val currentAccountRS: String, view: View):
            RecyclerView.ViewHolder(view), View.OnClickListener{

        private val transactionType: TextView = view.findViewById(R.id.transaction_type)
        private val transactionStatus: AppCompatImageView = view.findViewById(R.id.transaction_status_icon)
        private val transactionDetail: TextView = view.findViewById(R.id.transaction_detail)
        private val transactionAmount: TextView = view.findViewById(R.id.transaction_change)

        private  var transaction: Transaction? = null

        fun setTransaction(value: Transaction) {
            setTransactionDescription(value)
            this.transaction = value
        }

        init { view.setOnClickListener(this) }

        override fun onClick(view: View?) {
            val intent = Intent(this.activity, TransactionDetailActivity::class.java)
            intent.putExtra(TransactionDetailActivity.TRANSACTION_OBJECT_KEY, this.transaction!!)
            activity.startActivity(intent)
        }

        private fun setTransactionDescription(value: Transaction) {
            val transactionDescription = TransactionDescriptionFactory.getTransactionDescription(
                    this.currentAccountRS,
                    value)

            transactionType.text = transactionDescription.transactionType
            transactionDetail.text = transactionDescription.transactionDetail

            val amount = DisplayBalance.NQTConversion(transactionDescription.transactionChange)
            transactionAmount.text = DisplayBalance.format(amount)

            if (value.confirmations == null) {
                transactionStatus.setImageResource(R.drawable.ic_swap_vertical_circle)
            }
        }
    }
}
