package me.ibrohim.wallet.adapter

import me.ibrohim.wallet.network.nxt.Transaction

/**
 * @Author ibrohimislam
 */

class TransactionDescriptionFactory {

    companion object {
        private val TYPE_PAYMENT: Long = -2

        fun getTransactionDescription(accountRS: String, transaction: Transaction): TransactionDescription {
            return when (transaction.type) {
                TYPE_PAYMENT -> getTransactionPaymentDescription(accountRS, transaction)
                else -> TransactionDescription("Unknown Transaction", "", 0L)
            }
        }

        private fun getTransactionPaymentDescription(accountRS: String, transaction: Transaction): TransactionDescription {
            return if (accountRS == transaction.senderRS) {
                val description = "to ${transaction.recipientRS}"
                val change = -(transaction.amountNQT.toLong()  + transaction.feeNQT.toLong())
                TransactionDescription("Pay Out", description, change)
            } else {
                val description = "from ${transaction.senderRS}"
                val change = transaction.amountNQT.toLong()
                TransactionDescription("Pay In", description, change)
            }
        }

    }

    data class TransactionDescription (
            val transactionType: String,
            val transactionDetail: String,
            val transactionChange: Long
    )
}
