package me.ibrohim.wallet

import android.annotation.SuppressLint
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import me.ibrohim.wallet.adapter.TransactionDetailAdapter
import me.ibrohim.wallet.network.nxt.Transaction
import java.text.SimpleDateFormat
import java.util.*


class TransactionDetailActivity : AppCompatActivity() {

    companion object {
        val TRANSACTION_OBJECT_KEY = "transaction_object"
        @SuppressLint("SimpleDateFormat")
        val GENESIS_BLOCK_TIMESTAMP = SimpleDateFormat("yyyy-MM-dd HH:mm:ss Z")
                .parse("2018-02-10 12:00:00 +0700").time
    }

    private lateinit var transactionObject: Transaction
    private lateinit var transactionDetailPropertiesList: MutableList<Pair<String, String>>
    private lateinit var transactionDetailPropertiesListView: RecyclerView
    private lateinit var transactionDetailMainToolbar: Toolbar

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_transaction_detail)

        transactionObject = intent.getSerializableExtra(TRANSACTION_OBJECT_KEY) as Transaction

        transactionDetailPropertiesListView = findViewById(R.id.transaction_detail_properties)
        transactionDetailPropertiesListView.addItemDecoration(DividerItemDecoration(this, LinearLayoutManager.VERTICAL))
        transactionDetailPropertiesListView.layoutManager = LinearLayoutManager(applicationContext)

        transactionDetailMainToolbar = findViewById(R.id.transaction_detail_main_toolbar)
        setSupportActionBar(transactionDetailMainToolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)

        val timestamp = blockTimestampToUnixTimestamp(transactionObject.timestamp)

        transactionDetailPropertiesList = LinkedList()
        transactionDetailPropertiesList.add(Pair("Date", getDateFromUnixTimestamp(timestamp)))
        transactionDetailPropertiesList.add(Pair("Time", getTimeFromUnixTimestamp(timestamp)))
        if (transactionObject.senderRS != null) transactionDetailPropertiesList.add(Pair("Sender Address", transactionObject.senderRS))
        if (transactionObject.recipientRS != null)transactionDetailPropertiesList.add(Pair("Recipient Address", transactionObject.recipientRS))
        transactionDetailPropertiesList.add(Pair("Hash", transactionObject.fullHash))

        transactionDetailPropertiesListView.adapter = TransactionDetailAdapter(transactionDetailPropertiesList)
    }

    fun blockTimestampToUnixTimestamp(blockTimestamp: Long): Long {
        return blockTimestamp*1000 + GENESIS_BLOCK_TIMESTAMP
    }

    fun getTimeFromUnixTimestamp(timestamp: Long): String {
        val date = Date(timestamp)
        val formatter = SimpleDateFormat("HH:mm", Locale.ENGLISH)
        return formatter.format(date)
    }

    fun getDateFromUnixTimestamp(timestamp: Long): String {
        val date = Date(timestamp)
        val dayNumberSuffix = getDayNumberSuffix(date.date)
        val formatter = SimpleDateFormat("MMMM d'$dayNumberSuffix', yyyy", Locale.ENGLISH)
        return formatter.format(date)
    }

    private fun getDayNumberSuffix(day: Int): String {
        if (day in 11..13) {
            return "th"
        }
        return when (day % 10) {
            1 -> "st"
            2 -> "nd"
            3 -> "rd"
            else -> "th"
        }
    }
}
