package me.ibrohim.wallet.auth

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import kotlinx.android.synthetic.main.activity_pin.*
import android.content.IntentFilter
import android.support.v4.content.LocalBroadcastManager
import android.support.design.widget.Snackbar
import android.support.v4.content.ContextCompat
import android.widget.TextView
import android.view.animation.AlphaAnimation
import me.ibrohim.wallet.intro.IntroActivity
import me.ibrohim.wallet.MainActivity
import me.ibrohim.wallet.R
import me.ibrohim.wallet.SendActivity


/**
 * @author ibrohimislam
 * @version 12/26/17
 */

class AuthActivity : AppCompatActivity() {

    private val PIN_LENGTH = 6

    private val keyVault = KeyVault()
    private lateinit var pinCode : String

    override fun onResume() {
        super.onResume()
        pinClear()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pin)

        keyVault.initialize(applicationContext)
        if (keyVault.isEmpty()) {
            val intent = Intent(this, IntroActivity::class.java) // Call the AppIntro java class
            startActivity(intent)
            finish()
        }

        setTextInstruction()

        indicator_view.setPinLength(PIN_LENGTH)
        registerReceiver()
    }

    fun onKeyboardClick(v: View) {
        val button = v as Button

        if (v.id == R.id.button_erase) {
            pinErase()
        } else {
            pinAppend(button.text.toString())
        }

        if (this.pinCode.length == PIN_LENGTH) {
            processPin()
        }
    }

    private fun setTextInstruction() {
        instruction_text.text = "Type your security key"
    }

    private fun pinClear() {
        this.pinCode = ""
        indicator_view.refresh(0)
    }

    private fun pinErase() {
        val pinLength = this.pinCode.length
        if (pinLength > 0) {
            this.pinCode = this.pinCode.dropLast(1)
        }
        indicator_view.refresh(this.pinCode.length)
    }

    private fun pinAppend(string: String) {
        val pinLength = this.pinCode.length
        if (pinLength < PIN_LENGTH) {
            this.pinCode = this.pinCode + string
        }
        indicator_view.refresh(this.pinCode.length)
    }

    private fun processPin() {
        val serviceIntent = Intent(this, PassphraseService::class.java)
                .setAction(Intent.ACTION_VIEW)
                .putExtra(PassphraseService.PINCODE_KEY, this.pinCode)

        showProgress()
        this.startService(serviceIntent)
    }

    private fun showProgress() {
        val inAnimation = AlphaAnimation(0f, 1f)
        inAnimation.duration = 200
        progress_bar_holder.setOnTouchListener { _, _ -> true }
        progress_bar_holder.animation = inAnimation
        progress_bar_holder.visibility = View.VISIBLE
    }

    private fun hideProgress() {
        val outAnimation = AlphaAnimation(1f, 0f)
        outAnimation.duration = 200
        progress_bar_holder.setOnTouchListener { _, _ -> false }
        progress_bar_holder.animation = outAnimation
        progress_bar_holder.visibility = View.GONE
    }

    private fun registerReceiver() {
        val statusIntentFilter = IntentFilter(PassphraseService.BROADCAST_ACTION)
        val resultReceiver = ResultReceiver()
        LocalBroadcastManager.getInstance(this).registerReceiver(
                resultReceiver,
                statusIntentFilter)
    }

    private inner class ResultReceiver: BroadcastReceiver() {
        override fun onReceive(context: Context, resultIntent: Intent) {
            hideProgress()

            val statusString = resultIntent.getStringExtra(PassphraseService.STATUS_KEY)
            Log.d("RESULT", statusString)

            when (statusString) {
                "success" -> {
                    val passphrase = resultIntent.getStringExtra(PassphraseService.PASSPHRASE_KEY)
                    PassphraseManager.getInstance(applicationContext).passphrase = passphrase

                    val launchIntent = this@AuthActivity.intent
                    when (launchIntent.action) {
                        Intent.ACTION_MAIN -> launchMainActivity()
                        Intent.ACTION_VIEW -> {
                            val recipientAddress = launchIntent.data.getQueryParameter("recipient")
                            launchSendActivity(recipientAddress)
                        }
                    }
                }
                "failed" -> {

                    val snackbar = Snackbar.make(this@AuthActivity.constraintLayout,
                            "PIN salah", Snackbar.LENGTH_SHORT)

                    snackbar.view.setBackgroundColor(ContextCompat.getColor(this@AuthActivity, R.color.message_error_background))
                    snackbar.view.findViewById<TextView>(android.support.design.R.id.snackbar_text)
                        .setTextColor(ContextCompat.getColor(this@AuthActivity, R.color.message_error_text_color))
                    snackbar.show()

                    this@AuthActivity.pinClear()

                }
            }
        }
    }

    private fun launchMainActivity() {
        val mainActivityIntent = Intent(this@AuthActivity, MainActivity::class.java)
        mainActivityIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)

        this@AuthActivity.startActivity(mainActivityIntent)
        this@AuthActivity.finish()
    }

    private fun launchSendActivity(recipientAddress: String) {
        val mainActivityIntent = Intent(this@AuthActivity, SendActivity::class.java)
                .putExtra(SendActivity.ADDRESS_INTENT_KEY, recipientAddress)
                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)

        this@AuthActivity.startActivity(mainActivityIntent)
        this@AuthActivity.finish()
    }

}
