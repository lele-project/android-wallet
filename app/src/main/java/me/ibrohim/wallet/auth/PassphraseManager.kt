package me.ibrohim.wallet.auth

import android.annotation.SuppressLint
import android.content.Context

/**
 * @author ibrohimislam
 */

class PassphraseManager private constructor(private val context: Context) {

    var passphrase: String? = null

    companion object {

        @SuppressLint("StaticFieldLeak")
        private var instance: PassphraseManager? = null

        fun getInstance(context: Context): PassphraseManager {
            if (instance == null) {
                instance = PassphraseManager(context.applicationContext)
            }

            return instance as PassphraseManager
        }

    }
}
