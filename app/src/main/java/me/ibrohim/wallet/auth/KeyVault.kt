package me.ibrohim.wallet.auth

import android.content.Context
import android.content.SharedPreferences
import java.nio.charset.Charset
import java.security.SecureRandom
import javax.crypto.Cipher
import javax.crypto.SecretKey
import javax.crypto.spec.SecretKeySpec
import javax.crypto.SecretKeyFactory
import javax.crypto.spec.PBEKeySpec
import android.annotation.SuppressLint
import android.util.Base64
import me.ibrohim.wallet.Constants
import java.security.MessageDigest
import javax.crypto.spec.IvParameterSpec

/**
 * @author ibrohimislam
 */

class KeyVault {

    companion object {
        val CHARSET = Charset.forName("UTF-8")!!

        val PBKDF2_METHOD = "PBKDF2WithHmacSHA1"

        val ITERATION_COUNT = 7000
        val SALT_SIZE_BYTE = 256
        val KEY_LENGTH = 192

        val CIPHER_ALGORITHM = "DESede"
        val CIPHER_TRANSFORMATION = "DESede/CBC/PKCS5PADDING"
        val IV_SIZE_BYTE = 8


        val PREFERENCE_FILE: String = Constants.PREFERENCE_FILE
        val PREFERENCE_CIPHER: String = "cipher"
        val PREFERENCE_PASSPHRASE: String = "encrypted_passphrase"
        private val PREFERENCE_SALT: String = "salt"
        private val PREFERENCE_IV: String = "iv"
    }

    private lateinit var sharedPref: SharedPreferences
    private lateinit var cipherFingerprint: ByteArray

    fun initialize(context: Context) {
        sharedPref = context.getSharedPreferences(PREFERENCE_FILE, Context.MODE_PRIVATE)
        cipherFingerprint = getHash(
                "$PBKDF2_METHOD:$ITERATION_COUNT:$SALT_SIZE_BYTE:$CIPHER_TRANSFORMATION")
    }

    @SuppressLint("ApplySharedPref")
    fun putPassphrase(pin: String, passphrase: String) : String {
        val iv = generateRandomByte(IV_SIZE_BYTE)
        val salt = generateRandomByte(SALT_SIZE_BYTE)
        val key = explodeKey(pin.toCharArray(), salt)

        val plainText = passphrase.toByteArray(CHARSET)
        val cipherText = encrypt(key, iv, plainText)

        val editor = sharedPref.edit()
        write(PREFERENCE_CIPHER, cipherFingerprint, editor)
        write(PREFERENCE_IV, iv, editor)
        write(PREFERENCE_SALT, salt, editor)
        write(PREFERENCE_PASSPHRASE, cipherText, editor)
        editor.commit()

        return passphrase
    }

    @Throws(Exception::class)
    fun getPassphrase(pin: String) : String {
        val iv = read(PREFERENCE_IV)
        val salt = read(PREFERENCE_SALT)
        val key = explodeKey(pin.toCharArray(), salt)

        val cipherText = read(PREFERENCE_PASSPHRASE)
        val plainText = decrypt(key, iv, cipherText)

        return plainText.toString(CHARSET)
    }

    fun isEmpty(): Boolean {
        if (!sharedPref.getString(PREFERENCE_PASSPHRASE, "").isBlank()) {
            return !isEncryptionCompatible()
        }
        return true
    }

    private fun isEncryptionCompatible(): Boolean {
        val currentCipherFingerprint = read(PREFERENCE_CIPHER)
        return currentCipherFingerprint.contentEquals(cipherFingerprint)
    }

    private fun write(key: String, salt: ByteArray, editor: SharedPreferences.Editor) {
        val valueString = Base64.encodeToString(salt, Base64.DEFAULT)
        editor.putString(key, valueString)
    }

    private fun read(key: String): ByteArray {
        val saltString = sharedPref.getString(key, "")
        return Base64.decode(saltString, Base64.DEFAULT)
    }

    @Throws(Exception::class)
    private fun explodeKey(password: CharArray, salt: ByteArray): SecretKey {
        val f = SecretKeyFactory.getInstance(PBKDF2_METHOD)
        val ks = PBEKeySpec(password, salt, ITERATION_COUNT, KEY_LENGTH)
        return f.generateSecret(ks)
    }

    private fun generateRandomByte(size: Int): ByteArray {
        val random = SecureRandom()
        val bytes = ByteArray(size)
        random.nextBytes(bytes)
        return bytes
    }

    @Throws(Exception::class)
    private fun encrypt(key: SecretKey, iv: ByteArray, clear: ByteArray): ByteArray {
        val skeySpec = SecretKeySpec(key.encoded, CIPHER_ALGORITHM)
        val cipher = Cipher.getInstance(CIPHER_TRANSFORMATION)
        val ivParameterSpec = IvParameterSpec(iv)
        cipher.init(Cipher.ENCRYPT_MODE, skeySpec, ivParameterSpec)
        return cipher.doFinal(clear)
    }

    @Throws(Exception::class)
    private fun decrypt(key: SecretKey, iv: ByteArray, encrypted: ByteArray): ByteArray {
        val skeySpec = SecretKeySpec(key.encoded, CIPHER_ALGORITHM)
        val cipher = Cipher.getInstance(CIPHER_TRANSFORMATION)
        val ivParameterSpec = IvParameterSpec(iv)
        cipher.init(Cipher.DECRYPT_MODE, skeySpec, ivParameterSpec)
        return cipher.doFinal(encrypted)
    }

    @Throws(Exception::class)
    fun getHash(input: String): ByteArray {
        val digest: MessageDigest = MessageDigest.getInstance("SHA-256")
        digest.reset()
        return digest.digest(input.toByteArray())
    }

}