package me.ibrohim.wallet.auth

import android.content.Context
import android.graphics.drawable.Drawable
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RelativeLayout
import kotlinx.android.synthetic.main.view_pin_indicator.view.*

import java.util.ArrayList

import me.ibrohim.wallet.R
import android.util.TypedValue



/**
 * @author stoyan, oliviergoutay, and ibrohimislam
 * @version 12/26/17
 */

class PinIndicatorView @JvmOverloads constructor(_context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0) : RelativeLayout(_context, attrs, defStyleAttr) {

    private var indicatorViews: MutableList<ImageView>? = null

    var currentLength: Int = 0
        private set

    private var emptyDotDrawableId: Drawable? = null
    private var fullDotDrawableId: Drawable? = null

    init {
        initializeView(attrs, defStyleAttr)
    }

    private fun initializeView(attrs: AttributeSet?, defStyleAttr: Int) {
        if (attrs != null && !isInEditMode) {
            val attributes = this.context.theme.obtainStyledAttributes(attrs, R.styleable.PinIndicatorView,
                    defStyleAttr, 0)

            this.emptyDotDrawableId = attributes.getDrawable(R.styleable.PinIndicatorView_drawable_empty_dot)
            if (this.emptyDotDrawableId == null) {
                this.emptyDotDrawableId = resources.getDrawable(R.drawable.shape_round_empty)
            }
            this.fullDotDrawableId = attributes.getDrawable(R.styleable.PinIndicatorView_drawable_full_dot)
            if (this.fullDotDrawableId == null) {
                this.fullDotDrawableId = resources.getDrawable(R.drawable.shape_round_fill)
            }

            attributes.recycle()

            val inflater = this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            val view = inflater.inflate(R.layout.view_pin_indicator, this) as PinIndicatorView

            this.indicatorViews = ArrayList()
        }
    }

    /**
     * Refresh the [android.widget.ImageView]s to look like what typed the user
     *
     * @param pinLength the current pin code length typed by the user
     */
    fun refresh(pinLength: Int) {
        currentLength = pinLength
        for (i in indicatorViews!!.indices) {
            if (pinLength - 1 >= i) {
                indicatorViews!![i].setImageDrawable(fullDotDrawableId)
            } else {
                indicatorViews!![i].setImageDrawable(emptyDotDrawableId)
            }
        }
    }

    /**
     * Sets a custom empty dot drawable for the [ImageView]s.
     * @param drawable the resource Id for a custom drawable
     */
    fun setEmptyDotDrawable(drawable: Drawable) {
        emptyDotDrawableId = drawable
    }

    /**
     * Sets a custom full dot drawable for the [ImageView]s.
     * @param drawable the resource Id for a custom drawable
     */
    fun setFullDotDrawable(drawable: Drawable) {
        fullDotDrawableId = drawable
    }

    /**
     * Sets a custom empty dot drawable for the [ImageView]s.
     * @param drawableId the resource Id for a custom drawable
     */
    fun setEmptyDotDrawable(drawableId: Int) {
        emptyDotDrawableId = resources.getDrawable(drawableId)
    }

    /**
     * Sets a custom full dot drawable for the [ImageView]s.
     * @param drawableId the resource Id for a custom drawable
     */
    fun setFullDotDrawable(drawableId: Int) {
        fullDotDrawableId = resources.getDrawable(drawableId)
    }

    /**
     * Sets the length of the pin code.
     *
     * @param pinLength the length of the pin code
     */
    fun setPinLength(pinLength: Int) {

        indicator_container.removeAllViews()
        val temp = ArrayList<ImageView>(pinLength)

        for (i in 0 until pinLength) {
            val imageView: ImageView
            val layoutParams: LinearLayout.LayoutParams

            if (i < this.indicatorViews!!.size) {
                imageView = this.indicatorViews!![i]
                layoutParams = imageView.layoutParams as LinearLayout.LayoutParams

            } else {
                imageView = ImageView(this.context)
                imageView.requestLayout()

                layoutParams = LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)

                layoutParams.setMargins(dpToPx(5.0F), 0, dpToPx(5.0F), 0)

                layoutParams.height = dpToPx(10.0F)
                layoutParams.width = dpToPx(10.0F)
            }

            indicator_container.addView(imageView, layoutParams)
            temp.add(imageView)
        }

        this.indicatorViews!!.clear()
        this.indicatorViews!!.addAll(temp)
        refresh(0)
    }

    fun dpToPx(dp: Float): Int {
        val px = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, resources.displayMetrics)
        return px.toInt()
    }
}
