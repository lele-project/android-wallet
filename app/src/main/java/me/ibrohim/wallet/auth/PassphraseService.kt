package me.ibrohim.wallet.auth

import android.app.IntentService
import android.content.Intent
import javax.crypto.BadPaddingException
import android.support.v4.content.LocalBroadcastManager



/**
 * @author ibrohimislam
 */

class PassphraseService: IntentService("PassphraseService") {

    companion object {
        val BROADCAST_ACTION = "me.ibrohim.wallet.RESULT"

        val PINCODE_KEY = "me.ibrohim.wallet.PINCODE"
        val PASSPHRASE_KEY = "me.ibrohim.wallet.PASSPHRASE"
        val STATUS_KEY = "me.ibrohim.wallet.STATUS"
    }

    private val keyVault = KeyVault()
    private var passphrase: String = ""

    private fun putPassphrase(pinCode: String) {
        keyVault.putPassphrase(pinCode, this.passphrase)

        val localIntent = Intent(BROADCAST_ACTION)
                .putExtra(STATUS_KEY, "success")
                .putExtra(PASSPHRASE_KEY, this.passphrase)

        LocalBroadcastManager.getInstance(this).sendBroadcast(localIntent)
    }

    private fun getPassphrase(pinCode: String) {
        var localIntent : Intent

        try {
            this.passphrase = keyVault.getPassphrase(pinCode)

            localIntent = Intent(BROADCAST_ACTION)
                    .putExtra(STATUS_KEY, "success")
                    .putExtra(PASSPHRASE_KEY, this.passphrase)

        } catch (exception: BadPaddingException) {

            localIntent = Intent(BROADCAST_ACTION)
                    .putExtra(STATUS_KEY, "failed")

        }

        LocalBroadcastManager.getInstance(this).sendBroadcast(localIntent)
    }

    override fun onHandleIntent(intent: Intent) {
        keyVault.initialize(this.applicationContext)

        val action = intent.action
        val pinCode = intent.getStringExtra(PINCODE_KEY)

        when {
            action.equals(Intent.ACTION_VIEW, true) -> {
                this.getPassphrase(pinCode = pinCode)
            }
            action.equals(Intent.ACTION_EDIT, true) -> {
                passphrase = intent.getStringExtra(PASSPHRASE_KEY)
                this.putPassphrase(pinCode = pinCode)
            }
        }

    }
}
