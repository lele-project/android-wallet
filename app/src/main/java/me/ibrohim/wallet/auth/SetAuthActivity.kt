package me.ibrohim.wallet.auth

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import kotlinx.android.synthetic.main.activity_pin.*
import android.content.IntentFilter
import android.support.v4.content.LocalBroadcastManager
import android.support.design.widget.Snackbar
import android.support.v4.content.ContextCompat
import android.widget.TextView
import android.view.animation.AlphaAnimation
import me.ibrohim.wallet.MainActivity
import me.ibrohim.wallet.R


/**
 * @author ibrohimislam
 * @version 04/01/18
 */

class SetAuthActivity : AppCompatActivity() {

    private var state = STATE_SET_PIN
    companion object {
        val STATE_SET_PIN = "set"
        val STATE_CONFIRM_PIN = "cofirm"
    }

    private val PREF_FILE = "wallet"
    private val PREF_FIRST_TIME_LAUNCH_KEY = "first"
    private val PIN_LENGTH = 6

    private val keyVault = KeyVault()
    private var pinCode: String? = null
    private var inputtedPinCode: String = ""

    override fun onResume() {
        super.onResume()
        pinClear()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pin)

        keyVault.initialize(applicationContext)
        setTextInstruction()

        indicator_view.setPinLength(PIN_LENGTH)
        registerReceiver()
    }

    fun onKeyboardClick(v: View) {
        val button = v as Button

        if (v.id == R.id.button_erase) {
            pinErase()
        } else {
            pinAppend(button.text.toString())
        }

        if (this.inputtedPinCode.length == PIN_LENGTH) {
            processPin()
        }
    }

    private fun setTextInstruction() {
        if (state == STATE_SET_PIN) {
            instruction_text.text = "Set your security key"
        } else {
            instruction_text.text = "Confirm security key"
        }
    }

    private fun pinClear() {
        this.inputtedPinCode = ""
        indicator_view.refresh(0)
    }

    private fun pinErase() {
        val pinLength = this.inputtedPinCode.length
        if (pinLength > 0) {
            this.inputtedPinCode = this.inputtedPinCode.dropLast(1)
        }
        indicator_view.refresh(this.inputtedPinCode.length)
    }

    private fun pinAppend(string: String) {
        val pinLength = this.inputtedPinCode.length
        if (pinLength < PIN_LENGTH) {
            this.inputtedPinCode = this.inputtedPinCode + string
        }
        indicator_view.refresh(this.inputtedPinCode.length)
    }

    private fun processPin() {
        Log.d("AUTH", "pinCode: ${pinCode}")
        Log.d("AUTH", "inputtedPinCode: ${inputtedPinCode}")

        if (state == STATE_SET_PIN) {
            this.pinCode = this.inputtedPinCode
            this.pinClear()
            this.state = STATE_CONFIRM_PIN
        } else if (state == STATE_CONFIRM_PIN && this.inputtedPinCode == this.pinCode) {
            this.putPassphrase()
        } else if (state == STATE_CONFIRM_PIN && this.inputtedPinCode != this.pinCode) {
            this.state = STATE_SET_PIN
            showErrorMessage("Security code not match!")
        }

        setTextInstruction()
    }

    private fun putPassphrase() {
        val passphrase = intent.getStringExtra(PassphraseService.PASSPHRASE_KEY)
        Log.d("AUTH", "passphrase: $passphrase")

        val serviceIntent = Intent(this, PassphraseService::class.java)
                .setAction(Intent.ACTION_EDIT)
                .putExtra(PassphraseService.PINCODE_KEY, this.inputtedPinCode)
                .putExtra(PassphraseService.PASSPHRASE_KEY, passphrase)

        showProgress()
        this.startService(serviceIntent)
    }

    private fun showProgress() {
        val inAnimation = AlphaAnimation(0f, 1f)
        inAnimation.duration = 200
        progress_bar_holder.setOnTouchListener { _, _ -> true }
        progress_bar_holder.animation = inAnimation
        progress_bar_holder.visibility = View.VISIBLE
    }

    private fun hideProgress() {
        val outAnimation = AlphaAnimation(1f, 0f)
        outAnimation.duration = 200
        progress_bar_holder.setOnTouchListener { _, _ -> false }
        progress_bar_holder.animation = outAnimation
        progress_bar_holder.visibility = View.GONE
    }

    private fun registerReceiver() {
        val statusIntentFilter = IntentFilter(PassphraseService.BROADCAST_ACTION)
        val resultReceiver = ResultReceiver()
        LocalBroadcastManager.getInstance(this).registerReceiver(
                resultReceiver,
                statusIntentFilter)
    }

    private fun showErrorMessage(text: String) {
        val snackbar = Snackbar.make(this.constraintLayout, text, Snackbar.LENGTH_SHORT)

        snackbar.view.setBackgroundColor(ContextCompat.getColor(this, R.color.message_error_background))
        snackbar.view.findViewById<TextView>(android.support.design.R.id.snackbar_text)
                .setTextColor(ContextCompat.getColor(this, R.color.message_error_text_color))
        snackbar.show()

        this.pinClear()
    }

    private inner class ResultReceiver: BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            hideProgress()

            val mainActivityIntent = Intent(this@SetAuthActivity, MainActivity::class.java)
            mainActivityIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)

            this@SetAuthActivity.startActivity(mainActivityIntent)
            this@SetAuthActivity.finish()
        }
    }

}

