package me.ibrohim.wallet

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.graphics.Bitmap
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.Window
import android.widget.Toast
import com.google.zxing.BarcodeFormat
import com.google.zxing.EncodeHintType
import com.google.zxing.WriterException
import com.google.zxing.qrcode.QRCodeWriter
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel
import kotlinx.android.synthetic.main.dialog_receive.*
import java.util.*
import android.os.AsyncTask
import android.support.v4.content.ContextCompat.startActivity
import android.content.Intent
import android.net.Uri


/**
 * @author ibrohimislam
 */

class ReceiveDialog(private val activity: Activity, private val accountRS: String)
    : Dialog(activity), android.view.View.OnClickListener {

    companion object {
        private val WIDTH = 1024

        private val WHITE = Color.TRANSPARENT
        private val BLACK = Color.BLACK
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.dialog_receive)

        account_text.text = accountRS
        GenerateQRCodeTask(accountRS).execute()

        button_copy.setOnClickListener(this)
        button_share.setOnClickListener(this)
    }

    override fun onClick(view: View?) {
        when (view!!.id) {
            R.id.button_copy -> copyTextAccountToClipboard()
            R.id.button_share -> shareReceiveLink()
        }
    }

    private fun shareReceiveLink() {
        val receiveLink = buildReceiveLink()
        val shareText = "Send me LELE: $receiveLink"
        val sendIntent = Intent()
        sendIntent.action = Intent.ACTION_SEND
        sendIntent.putExtra(Intent.EXTRA_TEXT, shareText)
        sendIntent.type = "text/plain"
        startActivity(activity, sendIntent, null)
    }

    private fun buildReceiveLink(): String {
        val builder = Uri.Builder()
        builder.scheme("https")
                .authority("e-chain.id")
                .appendPath("wallet")
                .appendPath("send")
                .appendQueryParameter("recipient", this.accountRS)

        return builder.build().toString()
    }

    private fun copyTextAccountToClipboard() {
        setClipboardText("LELE address", this.accountRS)
        Toast.makeText(this.activity.applicationContext, "LELE address copied", Toast.LENGTH_SHORT).show()
    }

    private fun setClipboardText(label:String, text: String) {
        val clipboardManager = this.activity.application
                .getSystemService(Context.CLIPBOARD_SERVICE) as android.content.ClipboardManager
        val clipData = android.content.ClipData
                .newPlainText(label, text)
        clipboardManager.primaryClip = clipData
    }

    @Throws(WriterException::class)
    fun encodeAsBitmap(content: String): Bitmap? {
        var hints: MutableMap<EncodeHintType, Any> = EnumMap(EncodeHintType::class.java)
        hints.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.H)
        hints.put(EncodeHintType.MARGIN, 2)

        val encoding = guessAppropriateEncoding(content)
        if (encoding != null) {
            hints.put(EncodeHintType.CHARACTER_SET, encoding)
        }

        val writer = QRCodeWriter()
        val result = writer.encode(content, BarcodeFormat.QR_CODE, WIDTH, WIDTH, hints)
        val width = result.width
        val height = result.height
        val pixels = IntArray(width * height)

        for (y in 0 until height) {
            val offset = y * width
            for (x in 0 until width) {
                pixels[offset + x] = if (result.get(x, y)) BLACK else WHITE
            }
        }

        val bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888)
        bitmap.setPixels(pixels, 0, width, 0, 0, width, height)
        return bitmap
    }

    private fun guessAppropriateEncoding(contents: CharSequence): String? {
        // Very crude at the moment
        for (i in 0 until contents.length) {
            if (contents[i].toInt() > 0xFF) {
                return "UTF-8"
            }
        }
        return null
    }

    internal inner class GenerateQRCodeTask(private val accountRS: String) : AsyncTask<Void?, Void?, Void?>() {
        lateinit var qrCode : Bitmap

        override fun doInBackground(vararg arg: Void?): Void? {
            this.qrCode = encodeAsBitmap(accountRS)!!
            return null
        }

        override fun onPostExecute(result: Void?) {
            super.onPostExecute(result)
            progress_bar.visibility = View.GONE
            barcode.setImageBitmap(this.qrCode)
        }
    }
}
