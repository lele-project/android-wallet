package me.ibrohim.wallet

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity

import com.google.android.gms.vision.barcode.Barcode
import kotlinx.android.synthetic.main.activity_scan.*

import me.ibrohim.barcode.BarcodeReader

class ScanActivity : AppCompatActivity(), BarcodeReader.BarcodeReaderListener {

    companion object {
        val SCAN_QR_NOT_FOUND = 0
        val SCAN_QR_FOUND = 1

        val QR_DATA_STRING = "qr:data"
    }

    private var barcodeReader: BarcodeReader? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_scan)

        barcodeReader = supportFragmentManager.findFragmentById(R.id.barcode_scanner) as BarcodeReader
        barcodeReader!!.setListener(this)

        scannerClose.setOnClickListener { _ -> close() }
    }

    private fun close() {
        setResult(SCAN_QR_NOT_FOUND)
        finish()
    }

    override fun onScanned(barcode: Barcode) {
        val intent = Intent()
        intent.putExtra(QR_DATA_STRING, barcode.displayValue)

        setResult(SCAN_QR_FOUND, intent)
        finish()
    }
    override fun onScannedMultiple(list: List<Barcode>) {}
    override fun onCameraPermissionDenied() {}
}
