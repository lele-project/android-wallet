package me.ibrohim.wallet.utils

/**
 * @author ibrohimislam
 */


class DisplayBalance {

    companion object {
        fun NQTConversion(nqt: Long): Double {
            return nqt.toDouble() / 100000000
        }

        fun format(amount: Double): String {
            return if (amount - amount.dec() >= 0)
                String.format("%.0f", amount)
            else
                String.format("%.8f", amount)
        }
    }
}