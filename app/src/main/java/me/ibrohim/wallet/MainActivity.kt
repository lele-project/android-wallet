package me.ibrohim.wallet

import android.content.Context
import android.os.Bundle
import android.view.View
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.TextView
import android.widget.Toast
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_main.*
import me.ibrohim.wallet.auth.PassphraseService
import me.ibrohim.wallet.network.NXTApi
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.jackson.JacksonConverterFactory
import android.content.Intent
import android.support.design.widget.Snackbar
import android.view.animation.Animation.AnimationListener
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.DividerItemDecoration
import android.view.Menu
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.MenuItem
import com.google.firebase.crash.FirebaseCrash
import io.reactivex.Observable
import io.reactivex.functions.BiFunction
import me.ibrohim.wallet.utils.DisplayBalance
import me.ibrohim.wallet.adapter.TransactionArrayAdapter
import me.ibrohim.wallet.auth.PassphraseManager
import me.ibrohim.wallet.network.nxt.*
import java.util.*

class MainActivity : AppCompatActivity(), View.OnClickListener, SwipeRefreshLayout.OnRefreshListener {

    companion object {
        val SEND_REQUEST = 0x732f
    }

    private lateinit var nxt: NXTApi

    private lateinit var passphrase : String
    private lateinit var accountRS : String
    private var balance : Double = 0.0

    var isFabOpen: Boolean = false

    private lateinit var fab_open: Animation
    private lateinit var fab_close: Animation
    private lateinit var rotate_forward: Animation

    private lateinit var rotate_backward: Animation

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(main_toolbar)
        setupActionButton()

        this.passphrase = PassphraseManager.getInstance(applicationContext).passphrase!!

        this.nxt = Retrofit.Builder()
                .baseUrl("https://wallet.e-chain.id/")
                .addConverterFactory(JacksonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()
                .create(NXTApi::class.java)

        updateAccountData()

        list_transaction.addItemDecoration(DividerItemDecoration(this, LinearLayoutManager.VERTICAL))
        list_transaction.layoutManager = LinearLayoutManager(applicationContext)

        text_account.setOnClickListener(this)
        swipe_refresh.setOnRefreshListener(this)
    }

    private fun updateAccountData() {
        val sharedPref = applicationContext.getSharedPreferences(Constants.PREFERENCE_FILE, Context.MODE_PRIVATE)
        val accountRS = sharedPref.getString(Constants.PREFERENCE_ACCOUNTRS, "")

        if (accountRS.isEmpty()) {

            main_progress_bar_holder.visibility = View.VISIBLE

            nxt.getAccountId(this.passphrase)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({ account ->
                        val editor = sharedPref.edit()
                        editor.putString(Constants.PREFERENCE_ACCOUNTRS, account.accountRS)
                        editor.apply()

                        this.accountRS = account.accountRS
                        updateViews()

                        main_progress_bar_holder.visibility = View.GONE
                    }, ::onObservationError)

        } else {

            this.accountRS = accountRS
            updateViews()

        }
    }

    private fun setupActionButton() {
        fab_open = AnimationUtils.loadAnimation(applicationContext, R.anim.fab_open)
        fab_close = AnimationUtils.loadAnimation(applicationContext, R.anim.fab_close)
        rotate_forward = AnimationUtils.loadAnimation(applicationContext, R.anim.rotate_forward)
        rotate_backward = AnimationUtils.loadAnimation(applicationContext, R.anim.rotate_backward)

        button_action.setOnClickListener(this)
        button_action_send.setOnClickListener(this)
        button_action_receive.setOnClickListener(this)
    }

    private fun setupListTransaction(transactions: List<Transaction>) {

        if (list_transaction.adapter == null) {
            list_transaction.adapter = TransactionArrayAdapter(this, this.accountRS, transactions.toTypedArray())
        } else {
            val adapter = (list_transaction.adapter) as TransactionArrayAdapter
            adapter.values = transactions.toTypedArray()
            adapter.notifyDataSetChanged()
        }

        if (transactions.isEmpty()) {
            empty_list_view.visibility = View.VISIBLE
            list_transaction.visibility = View.GONE

            Log.d("TRANSACTION", "empty")
        } else {
            list_transaction.visibility = View.VISIBLE
            empty_list_view.visibility = View.GONE

            Log.d("TRANSACTION", "not empty")
        }

    }

    override fun onClick(view: View?) {
        when (view!!.id) {
            R.id.button_action ->
                animateFab()
            R.id.button_action_send ->
                closeFabThen(this::startSendActivity)
            R.id.button_action_receive ->
                closeFabThen(this::startReceiveDialog)
            R.id.text_account ->
                handleTextAccountClick(view as TextView)
        }
    }

    private fun startReceiveDialog() {
        ReceiveDialog(this, this.accountRS).show()
    }

    private fun startSendActivity() {balance
        val intent = Intent(this@MainActivity, SendActivity::class.java)
        intent.putExtra(PassphraseService.PASSPHRASE_KEY, this.passphrase)
        startActivityForResult(intent, SEND_REQUEST)
    }

    private fun setClipboardText(label:String, text: String) {
        val clipboardManager = application
                .getSystemService(Context.CLIPBOARD_SERVICE) as android.content.ClipboardManager
        val clipData = android.content.ClipData
                .newPlainText(label, text)
        clipboardManager.primaryClip = clipData
    }

    private fun handleTextAccountClick(view: TextView) {
        setClipboardText("LELE address", this.accountRS)
        Toast.makeText(applicationContext, "LELE address copied", Toast.LENGTH_SHORT).show()
    }

    private fun animateFab() {

        if (isFabOpen) {

            button_action.startAnimation(rotate_backward)
            layout_action_send.startAnimation(fab_close)
            layout_action_receive.startAnimation(fab_close)
            button_action_send.isClickable = false
            button_action_receive.isClickable = false
            isFabOpen = false

        } else {

            button_action.startAnimation(rotate_forward)
            layout_action_send.startAnimation(fab_open)
            layout_action_receive.startAnimation(fab_open)
            button_action_send.isClickable = true
            button_action_receive.isClickable = true
            isFabOpen = true

        }
    }

    private fun closeFabThen(callback: () -> Unit) {
        isFabOpen = false

        val closeFab = AnimationUtils.loadAnimation(applicationContext, R.anim.rotate_backward)
        closeFab.setAnimationListener(object : AnimationListener {
            override fun onAnimationEnd(animation: Animation) = callback()
            override fun onAnimationRepeat(animation: Animation?) {}
            override fun onAnimationStart(animation: Animation?) {}
        })

        button_action.startAnimation(closeFab)
        layout_action_send.startAnimation(fab_close)
        layout_action_receive.startAnimation(fab_close)

    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_tes, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_show_receive_dialog -> {
                startReceiveDialog()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onRefresh() {
        updateViews()
    }

    private fun updateViews() {
        updateAccountStatus()
        updateBalance()
        updateTransactions()
        text_account.text = this.accountRS
    }

    private fun updateAccountStatus() {
        nxt.getAccount(this.accountRS)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe ({ response ->
                    if (response.currentLesseeRS != null) {
                        text_account_status.visibility = View.VISIBLE
                    }
                }, ::onObservationError)
    }

    private fun updateBalance() {
        nxt.getBalance(this.accountRS)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe ({ response ->
                    this.balance = DisplayBalance.NQTConversion(response.balanceNQT.toLong())
                    text_balance.text =  "${DisplayBalance.format(this.balance)} LELE"
                }, ::onObservationError)
    }

    private fun updateTransactions() {
        val b = nxt.getBlockchainTransactions(this.accountRS)
        val c = nxt.getUnconfirmedTransactions(this.accountRS)

        Observable.zip(b, c, BiFunction<BlockchainTransactionsResponse, UnconfirmedTransactionsResponse, List<Transaction>> {
            d, t ->
            val l = LinkedList<Transaction>()
            l.addAll(t.unconfirmedTransactions)
            l.addAll(d.transactions)
            l
        }).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe ({ response ->
                    swipe_refresh.isRefreshing = false
                    setupListTransaction(response)
                }, ::onObservationError)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when (requestCode) {
            SEND_REQUEST -> {
                swipe_refresh.isRefreshing = true
                onRefresh()
            }
        }
    }

    private fun onObservationError(error: Throwable){
        FirebaseCrash.report(error)

        Snackbar.make(main_activity_coordinator_layout,
                "NETWORK ERROR", Snackbar.LENGTH_SHORT)
                .show()
    }
}
