package me.ibrohim.wallet.network

import io.reactivex.Observable
import me.ibrohim.wallet.network.nxt.*
import retrofit2.http.GET
import retrofit2.http.Query
import retrofit2.http.POST

interface NXTApi {
    @GET("nxt?requestType=getAccountId")
    fun getAccountId(@Query("secretPhrase") passphrase: String): Observable<AccountIdResponse>

    @GET("nxt?requestType=getAccount")
    fun getAccount(@Query("account") account: String): Observable<AccountResponse>

    @GET("nxt?requestType=getBalance&chain=1")
    fun getBalance(@Query("account") account: String): Observable<AccountBalanceResponse>

    @GET("nxt?requestType=getBlockchainTransactions&chain=1")
    fun getBlockchainTransactions(@Query("account") account: String): Observable<BlockchainTransactionsResponse>

    @GET("nxt?requestType=getUnconfirmedTransactions&chain=1")
    fun getUnconfirmedTransactions(@Query("account") account: String): Observable<UnconfirmedTransactionsResponse>

    @POST("nxt?requestType=sendMoney&chain=1&feeNQT=-1&feeRateNQTPerFXT=-1&broadcast=false")
    fun checkFee(@Query("secretPhrase") secretPhrase: String,
                  @Query("recipient") recipient: String,
                  @Query("amountNQT") amountNQT: String): Observable<SendMoneyResponse>

    @POST("nxt?requestType=sendMoney&chain=1")
    fun sendMoney(@Query("secretPhrase") secretPhrase: String,
                  @Query("recipient") recipient: String,
                  @Query("amountNQT") amountNQT: String,
                  @Query("feeNQT") feeNQT: String): Observable<SendMoneyResponse>
}
