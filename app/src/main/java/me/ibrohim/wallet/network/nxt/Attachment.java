
package me.ibrohim.wallet.network.nxt;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "currency",
    "units",
    "version.ExchangeBuy",
    "rateNQT",
    "version.OrdinaryPayment"
})
public class Attachment implements Serializable {

    @JsonProperty("currency")
    private String currency;
    @JsonProperty("units")
    private String units;
    @JsonProperty("version.ExchangeBuy")
    private long versionExchangeBuy;
    @JsonProperty("rateNQT")
    private String rateNQT;
    @JsonProperty("version.OrdinaryPayment")
    private long versionOrdinaryPayment;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("currency")
    public String getCurrency() {
        return currency;
    }

    @JsonProperty("currency")
    public void setCurrency(String currency) {
        this.currency = currency;
    }

    @JsonProperty("units")
    public String getUnits() {
        return units;
    }

    @JsonProperty("units")
    public void setUnits(String units) {
        this.units = units;
    }

    @JsonProperty("version.ExchangeBuy")
    public long getVersionExchangeBuy() {
        return versionExchangeBuy;
    }

    @JsonProperty("version.ExchangeBuy")
    public void setVersionExchangeBuy(long versionExchangeBuy) {
        this.versionExchangeBuy = versionExchangeBuy;
    }

    @JsonProperty("rateNQT")
    public String getRateNQT() {
        return rateNQT;
    }

    @JsonProperty("rateNQT")
    public void setRateNQT(String rateNQT) {
        this.rateNQT = rateNQT;
    }

    @JsonProperty("version.OrdinaryPayment")
    public long getVersionOrdinaryPayment() {
        return versionOrdinaryPayment;
    }

    @JsonProperty("version.OrdinaryPayment")
    public void setVersionOrdinaryPayment(long versionOrdinaryPayment) {
        this.versionOrdinaryPayment = versionOrdinaryPayment;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
