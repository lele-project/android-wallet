
package me.ibrohim.wallet.network.nxt;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "signatureHash",
    "transactionJSON",
    "unsignedTransactionBytes",
    "broadcasted",
    "requestProcessingTime",
    "transactionBytes",
    "fullHash",
    "transaction"
})
public class SendMoneyResponse {

    @JsonProperty("signatureHash")
    private String signatureHash;
    @JsonProperty("transactionJSON")
    private Transaction transactionJSON;
    @JsonProperty("unsignedTransactionBytes")
    private String unsignedTransactionBytes;
    @JsonProperty("broadcasted")
    private boolean broadcasted;
    @JsonProperty("requestProcessingTime")
    private long requestProcessingTime;
    @JsonProperty("transactionBytes")
    private String transactionBytes;
    @JsonProperty("fullHash")
    private String fullHash;
    @JsonProperty("transaction")
    private String transaction;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("signatureHash")
    public String getSignatureHash() {
        return signatureHash;
    }

    @JsonProperty("signatureHash")
    public void setSignatureHash(String signatureHash) {
        this.signatureHash = signatureHash;
    }

    @JsonProperty("transactionJSON")
    public Transaction getTransactionJSON() {
        return transactionJSON;
    }

    @JsonProperty("transactionJSON")
    public void setTransactionJSON(Transaction transactionJSON) {
        this.transactionJSON = transactionJSON;
    }

    @JsonProperty("unsignedTransactionBytes")
    public String getUnsignedTransactionBytes() {
        return unsignedTransactionBytes;
    }

    @JsonProperty("unsignedTransactionBytes")
    public void setUnsignedTransactionBytes(String unsignedTransactionBytes) {
        this.unsignedTransactionBytes = unsignedTransactionBytes;
    }

    @JsonProperty("broadcasted")
    public boolean isBroadcasted() {
        return broadcasted;
    }

    @JsonProperty("broadcasted")
    public void setBroadcasted(boolean broadcasted) {
        this.broadcasted = broadcasted;
    }

    @JsonProperty("requestProcessingTime")
    public long getRequestProcessingTime() {
        return requestProcessingTime;
    }

    @JsonProperty("requestProcessingTime")
    public void setRequestProcessingTime(long requestProcessingTime) {
        this.requestProcessingTime = requestProcessingTime;
    }

    @JsonProperty("transactionBytes")
    public String getTransactionBytes() {
        return transactionBytes;
    }

    @JsonProperty("transactionBytes")
    public void setTransactionBytes(String transactionBytes) {
        this.transactionBytes = transactionBytes;
    }

    @JsonProperty("fullHash")
    public String getFullHash() {
        return fullHash;
    }

    @JsonProperty("fullHash")
    public void setFullHash(String fullHash) {
        this.fullHash = fullHash;
    }

    @JsonProperty("transaction")
    public String getTransaction() {
        return transaction;
    }

    @JsonProperty("transaction")
    public void setTransaction(String transaction) {
        this.transaction = transaction;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
