
package me.ibrohim.wallet.network.nxt;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "unconfirmedBalanceNQT",
    "currentLessee",
    "currentLeasingHeightTo",
    "currentLesseeRS",
    "accountRS",
    "forgedBalanceNQT",
    "balanceNQT",
    "publicKey",
    "requestProcessingTime",
    "account",
    "currentLeasingHeightFrom"
})
public class AccountResponse {

    @JsonProperty("unconfirmedBalanceNQT")
    private String unconfirmedBalanceNQT;
    @JsonProperty("currentLessee")
    private String currentLessee;
    @JsonProperty("currentLeasingHeightTo")
    private long currentLeasingHeightTo;
    @JsonProperty("currentLesseeRS")
    private String currentLesseeRS;
    @JsonProperty("accountRS")
    private String accountRS;
    @JsonProperty("forgedBalanceNQT")
    private String forgedBalanceNQT;
    @JsonProperty("balanceNQT")
    private String balanceNQT;
    @JsonProperty("publicKey")
    private String publicKey;
    @JsonProperty("requestProcessingTime")
    private long requestProcessingTime;
    @JsonProperty("account")
    private String account;
    @JsonProperty("currentLeasingHeightFrom")
    private long currentLeasingHeightFrom;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("unconfirmedBalanceNQT")
    public String getUnconfirmedBalanceNQT() {
        return unconfirmedBalanceNQT;
    }

    @JsonProperty("unconfirmedBalanceNQT")
    public void setUnconfirmedBalanceNQT(String unconfirmedBalanceNQT) {
        this.unconfirmedBalanceNQT = unconfirmedBalanceNQT;
    }

    @JsonProperty("currentLessee")
    public String getCurrentLessee() {
        return currentLessee;
    }

    @JsonProperty("currentLessee")
    public void setCurrentLessee(String currentLessee) {
        this.currentLessee = currentLessee;
    }

    @JsonProperty("currentLeasingHeightTo")
    public long getCurrentLeasingHeightTo() {
        return currentLeasingHeightTo;
    }

    @JsonProperty("currentLeasingHeightTo")
    public void setCurrentLeasingHeightTo(long currentLeasingHeightTo) {
        this.currentLeasingHeightTo = currentLeasingHeightTo;
    }

    @JsonProperty("currentLesseeRS")
    public String getCurrentLesseeRS() {
        return currentLesseeRS;
    }

    @JsonProperty("currentLesseeRS")
    public void setCurrentLesseeRS(String currentLesseeRS) {
        this.currentLesseeRS = currentLesseeRS;
    }

    @JsonProperty("accountRS")
    public String getAccountRS() {
        return accountRS;
    }

    @JsonProperty("accountRS")
    public void setAccountRS(String accountRS) {
        this.accountRS = accountRS;
    }

    @JsonProperty("forgedBalanceNQT")
    public String getForgedBalanceNQT() {
        return forgedBalanceNQT;
    }

    @JsonProperty("forgedBalanceNQT")
    public void setForgedBalanceNQT(String forgedBalanceNQT) {
        this.forgedBalanceNQT = forgedBalanceNQT;
    }

    @JsonProperty("balanceNQT")
    public String getBalanceNQT() {
        return balanceNQT;
    }

    @JsonProperty("balanceNQT")
    public void setBalanceNQT(String balanceNQT) {
        this.balanceNQT = balanceNQT;
    }

    @JsonProperty("publicKey")
    public String getPublicKey() {
        return publicKey;
    }

    @JsonProperty("publicKey")
    public void setPublicKey(String publicKey) {
        this.publicKey = publicKey;
    }

    @JsonProperty("requestProcessingTime")
    public long getRequestProcessingTime() {
        return requestProcessingTime;
    }

    @JsonProperty("requestProcessingTime")
    public void setRequestProcessingTime(long requestProcessingTime) {
        this.requestProcessingTime = requestProcessingTime;
    }

    @JsonProperty("account")
    public String getAccount() {
        return account;
    }

    @JsonProperty("account")
    public void setAccount(String account) {
        this.account = account;
    }

    @JsonProperty("currentLeasingHeightFrom")
    public long getCurrentLeasingHeightFrom() {
        return currentLeasingHeightFrom;
    }

    @JsonProperty("currentLeasingHeightFrom")
    public void setCurrentLeasingHeightFrom(long currentLeasingHeightFrom) {
        this.currentLeasingHeightFrom = currentLeasingHeightFrom;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
