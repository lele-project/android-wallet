package me.ibrohim.wallet.network.nxt;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "unconfirmedBalanceNQT",
        "forgedBalanceNQT",
        "balanceNQT",
        "requestProcessingTime"
})
public class AccountBalanceResponse {

    @JsonProperty("unconfirmedBalanceNQT")
    private String unconfirmedBalanceNQT;
    @JsonProperty("forgedBalanceNQT")
    private String forgedBalanceNQT;
    @JsonProperty("balanceNQT")
    private String balanceNQT;
    @JsonProperty("requestProcessingTime")
    private long requestProcessingTime;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("unconfirmedBalanceNQT")
    public String getUnconfirmedBalanceNQT() {
        return unconfirmedBalanceNQT;
    }

    @JsonProperty("unconfirmedBalanceNQT")
    public void setUnconfirmedBalanceNQT(String unconfirmedBalanceNQT) {
        this.unconfirmedBalanceNQT = unconfirmedBalanceNQT;
    }

    @JsonProperty("forgedBalanceNQT")
    public String getForgedBalanceNQT() {
        return forgedBalanceNQT;
    }

    @JsonProperty("forgedBalanceNQT")
    public void setForgedBalanceNQT(String forgedBalanceNQT) {
        this.forgedBalanceNQT = forgedBalanceNQT;
    }

    @JsonProperty("balanceNQT")
    public String getBalanceNQT() {
        return balanceNQT;
    }

    @JsonProperty("balanceNQT")
    public void setBalanceNQT(String balanceNQT) {
        this.balanceNQT = balanceNQT;
    }

    @JsonProperty("requestProcessingTime")
    public long getRequestProcessingTime() {
        return requestProcessingTime;
    }

    @JsonProperty("requestProcessingTime")
    public void setRequestProcessingTime(long requestProcessingTime) {
        this.requestProcessingTime = requestProcessingTime;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
