
package me.ibrohim.wallet.network.nxt;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import io.reactivex.annotations.Nullable;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "senderPublicKey",
    "signature",
    "feeNQT",
    "transactionIndex",
    "type",
    "confirmations",
    "fullHash",
    "version",
    "phased",
    "ecBlockId",
    "signatureHash",
    "attachment",
    "senderRS",
    "subtype",
    "amountNQT",
    "sender",
    "ecBlockHeight",
    "block",
    "blockTimestamp",
    "deadline",
    "transaction",
    "timestamp",
    "height",
    "recipientRS",
    "recipient"
})


public class Transaction implements Serializable {

    @JsonProperty("senderPublicKey")
    private String senderPublicKey;
    @JsonProperty("signature")
    private String signature;
    @JsonProperty("feeNQT")
    private String feeNQT;
    @JsonProperty("transactionIndex")
    private long transactionIndex;
    @JsonProperty("type")
    private long type;
    @JsonProperty("confirmations")
    private Long confirmations;
    @JsonProperty("fullHash")
    private String fullHash;
    @JsonProperty("version")
    private long version;
    @JsonProperty("phased")
    private boolean phased;
    @JsonProperty("ecBlockId")
    private String ecBlockId;
    @JsonProperty("signatureHash")
    private String signatureHash;
    @JsonProperty("attachment")
    private Attachment attachment;
    @JsonProperty("senderRS")
    private String senderRS;
    @JsonProperty("subtype")
    private long subtype;
    @JsonProperty("amountNQT")
    private String amountNQT;
    @JsonProperty("sender")
    private String sender;
    @JsonProperty("ecBlockHeight")
    private long ecBlockHeight;
    @JsonProperty("block")
    private String block;
    @JsonProperty("blockTimestamp")
    private long blockTimestamp;
    @JsonProperty("deadline")
    private long deadline;
    @JsonProperty("transaction")
    private String transaction;
    @JsonProperty("timestamp")
    private long timestamp;
    @JsonProperty("height")
    private long height;
    @JsonProperty("recipientRS")
    private String recipientRS;
    @JsonProperty("recipient")
    private String recipient;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("senderPublicKey")
    public String getSenderPublicKey() {
        return senderPublicKey;
    }

    @JsonProperty("senderPublicKey")
    public void setSenderPublicKey(String senderPublicKey) {
        this.senderPublicKey = senderPublicKey;
    }

    @JsonProperty("signature")
    public String getSignature() {
        return signature;
    }

    @JsonProperty("signature")
    public void setSignature(String signature) {
        this.signature = signature;
    }

    @JsonProperty("feeNQT")
    public String getFeeNQT() {
        return feeNQT;
    }

    @JsonProperty("feeNQT")
    public void setFeeNQT(String feeNQT) {
        this.feeNQT = feeNQT;
    }

    @JsonProperty("transactionIndex")
    public long getTransactionIndex() {
        return transactionIndex;
    }

    @JsonProperty("transactionIndex")
    public void setTransactionIndex(long transactionIndex) {
        this.transactionIndex = transactionIndex;
    }

    @JsonProperty("type")
    public long getType() {
        return type;
    }

    @JsonProperty("type")
    public void setType(long type) {
        this.type = type;
    }

    @JsonProperty("confirmations")
    public Long getConfirmations() {
        return confirmations;
    }

    @JsonProperty("confirmations")
    public void setConfirmations(long confirmations) {
        this.confirmations = confirmations;
    }

    @JsonProperty("fullHash")
    public String getFullHash() {
        return fullHash;
    }

    @JsonProperty("fullHash")
    public void setFullHash(String fullHash) {
        this.fullHash = fullHash;
    }

    @JsonProperty("version")
    public long getVersion() {
        return version;
    }

    @JsonProperty("version")
    public void setVersion(long version) {
        this.version = version;
    }

    @JsonProperty("phased")
    public boolean isPhased() {
        return phased;
    }

    @JsonProperty("phased")
    public void setPhased(boolean phased) {
        this.phased = phased;
    }

    @JsonProperty("ecBlockId")
    public String getEcBlockId() {
        return ecBlockId;
    }

    @JsonProperty("ecBlockId")
    public void setEcBlockId(String ecBlockId) {
        this.ecBlockId = ecBlockId;
    }

    @JsonProperty("signatureHash")
    public String getSignatureHash() {
        return signatureHash;
    }

    @JsonProperty("signatureHash")
    public void setSignatureHash(String signatureHash) {
        this.signatureHash = signatureHash;
    }

    @JsonProperty("attachment")
    public Attachment getAttachment() {
        return attachment;
    }

    @JsonProperty("attachment")
    public void setAttachment(Attachment attachment) {
        this.attachment = attachment;
    }

    @JsonProperty("senderRS")
    public String getSenderRS() {
        return senderRS;
    }

    @JsonProperty("senderRS")
    public void setSenderRS(String senderRS) {
        this.senderRS = senderRS;
    }

    @JsonProperty("subtype")
    public long getSubtype() {
        return subtype;
    }

    @JsonProperty("subtype")
    public void setSubtype(long subtype) {
        this.subtype = subtype;
    }

    @JsonProperty("amountNQT")
    public String getAmountNQT() {
        return amountNQT;
    }

    @JsonProperty("amountNQT")
    public void setAmountNQT(String amountNQT) {
        this.amountNQT = amountNQT;
    }

    @JsonProperty("sender")
    public String getSender() {
        return sender;
    }

    @JsonProperty("sender")
    public void setSender(String sender) {
        this.sender = sender;
    }

    @JsonProperty("ecBlockHeight")
    public long getEcBlockHeight() {
        return ecBlockHeight;
    }

    @JsonProperty("ecBlockHeight")
    public void setEcBlockHeight(long ecBlockHeight) {
        this.ecBlockHeight = ecBlockHeight;
    }

    @JsonProperty("block")
    public String getBlock() {
        return block;
    }

    @JsonProperty("block")
    public void setBlock(String block) {
        this.block = block;
    }

    @JsonProperty("blockTimestamp")
    public long getBlockTimestamp() {
        return blockTimestamp;
    }

    @JsonProperty("blockTimestamp")
    public void setBlockTimestamp(long blockTimestamp) {
        this.blockTimestamp = blockTimestamp;
    }

    @JsonProperty("deadline")
    public long getDeadline() {
        return deadline;
    }

    @JsonProperty("deadline")
    public void setDeadline(long deadline) {
        this.deadline = deadline;
    }

    @JsonProperty("transaction")
    public String getTransaction() {
        return transaction;
    }

    @JsonProperty("transaction")
    public void setTransaction(String transaction) {
        this.transaction = transaction;
    }

    @JsonProperty("timestamp")
    public long getTimestamp() {
        return timestamp;
    }

    @JsonProperty("timestamp")
    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    @JsonProperty("height")
    public long getHeight() {
        return height;
    }

    @JsonProperty("height")
    public void setHeight(long height) {
        this.height = height;
    }

    @JsonProperty("recipientRS")
    public String getRecipientRS() {
        return recipientRS;
    }

    @JsonProperty("recipientRS")
    public void setRecipientRS(String recipientRS) {
        this.recipientRS = recipientRS;
    }

    @JsonProperty("recipient")
    public String getRecipient() {
        return recipient;
    }

    @JsonProperty("recipient")
    public void setRecipient(String recipient) {
        this.recipient = recipient;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
