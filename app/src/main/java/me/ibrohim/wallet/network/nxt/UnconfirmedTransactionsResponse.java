package me.ibrohim.wallet.network.nxt;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "requestProcessingTime",
        "unconfirmedTransactions"
})
public class UnconfirmedTransactionsResponse {

    @JsonProperty("requestProcessingTime")
    private long requestProcessingTime;
    @JsonProperty("unconfirmedTransactions")
    private List<Transaction> transactions = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("requestProcessingTime")
    public long getRequestProcessingTime() {
        return requestProcessingTime;
    }

    @JsonProperty("requestProcessingTime")
    public void setRequestProcessingTime(long requestProcessingTime) {
        this.requestProcessingTime = requestProcessingTime;
    }

    @JsonProperty("unconfirmedTransactions")
    public List<Transaction> getUnconfirmedTransactions() {
        return transactions;
    }

    @JsonProperty("unconfirmedTransactions")
    public void setUnconfirmedTransactions(List<Transaction> transactions) {
        this.transactions = transactions;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}

