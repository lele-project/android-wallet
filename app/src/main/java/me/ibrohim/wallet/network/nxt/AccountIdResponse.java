package me.ibrohim.wallet.network.nxt;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "accountRS",
        "publicKey",
        "requestProcessingTime",
        "account"
})
public class AccountIdResponse {

    @JsonProperty("accountRS")
    private String accountRS;
    @JsonProperty("publicKey")
    private String publicKey;
    @JsonProperty("requestProcessingTime")
    private long requestProcessingTime;
    @JsonProperty("account")
    private String account;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("accountRS")
    public String getAccountRS() {
        return accountRS;
    }

    @JsonProperty("accountRS")
    public void setAccountRS(String accountRS) {
        this.accountRS = accountRS;
    }

    @JsonProperty("publicKey")
    public String getPublicKey() {
        return publicKey;
    }

    @JsonProperty("publicKey")
    public void setPublicKey(String publicKey) {
        this.publicKey = publicKey;
    }

    @JsonProperty("requestProcessingTime")
    public long getRequestProcessingTime() {
        return requestProcessingTime;
    }

    @JsonProperty("requestProcessingTime")
    public void setRequestProcessingTime(long requestProcessingTime) {
        this.requestProcessingTime = requestProcessingTime;
    }

    @JsonProperty("account")
    public String getAccount() {
        return account;
    }

    @JsonProperty("account")
    public void setAccount(String account) {
        this.account = account;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}