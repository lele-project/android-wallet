package me.ibrohim.wallet

import android.os.Bundle
import android.view.View
import android.content.Intent
import android.support.design.widget.Snackbar
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.widget.TextView
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_send.*
import me.ibrohim.wallet.auth.PassphraseManager
import me.ibrohim.wallet.network.NXTApi
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.jackson.JacksonConverterFactory
import android.app.Activity
import android.view.inputmethod.InputMethodManager
import android.support.v4.app.NavUtils
import android.support.v4.app.TaskStackBuilder
import android.view.ViewGroup
import android.widget.Button
import com.crashlytics.android.Crashlytics
import me.ibrohim.wallet.network.nxt.SendMoneyResponse


class SendActivity : AppCompatActivity(), View.OnClickListener {

    companion object {
        val ADDRESS_INTENT_KEY = "address"
        val ADDRESS_SCAN_REQUEST = 0x8b23

        val RESULT_FAILED = 0x1232
        val RESULT_SENT = 0
    }

    private lateinit var nxt: NXTApi;

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_send)

        this.nxt = Retrofit.Builder()
                .baseUrl("https://wallet.e-chain.id/")
                .addConverterFactory(JacksonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()
                .create(NXTApi::class.java)

        buttonScan.setOnClickListener(this)
        buttonSend.setOnClickListener(this)
        buttonBack.setOnClickListener(this)

        editTextAddress.setText(intent.getStringExtra(ADDRESS_INTENT_KEY))
    }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.buttonScan ->
                startActivityForResult(Intent(this, ScanActivity::class.java), ADDRESS_SCAN_REQUEST)
            R.id.buttonSend ->
                handleButtonSendClick()
            R.id.buttonBack ->
                backToMainActivity()
        }
    }

    private fun handleButtonSendClick() {
        val address = editTextAddress.text.toString()
        val amountString = editTextAmount.text.toString()

        if (!isValidAddress(address)) {
            showErrorMessage("Invalid address")
            return
        }

        if (!isValidAmount(amountString)) {
            showErrorMessage("Invalid amount")
            return
        }

        startSend(address, amountString.toDouble())
    }

    private fun showErrorMessage(message: String) {
        hideInputMethod()

        val snackbar = Snackbar.make(layout_send_activity, message, Snackbar.LENGTH_SHORT)
        snackbar.view.setBackgroundColor(ContextCompat.getColor(this, R.color.message_error_background))
        snackbar.view.findViewById<TextView>(android.support.design.R.id.snackbar_text)
                .setTextColor(ContextCompat.getColor(this, R.color.message_error_text_color))
        snackbar.show()
    }

    private fun hideInputMethod() {
        val imm = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(layout_send_activity.windowToken, 0)
    }

    private fun isValidAmount(amountString: String): Boolean {
        return !amountString.isEmpty()
    }

    private fun isValidAddress(address: String): Boolean {
        return address.length == 25
    }

    private fun startSend(address: String, amount: Double) {
        val passphrase = PassphraseManager.getInstance(applicationContext).passphrase!!
        val nqt : Long = (amount*100000000).toLong()

        progress_bar_holder_send.visibility = View.VISIBLE

        val requestFee = this.nxt.checkFee(
                secretPhrase = passphrase,
                amountNQT = nqt.toString(),
                recipient = address)

        requestFee.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe ({ response ->

                    val feeNQT = response.transactionJSON.feeNQT
                    val request = this.nxt.sendMoney(
                            secretPhrase = passphrase,
                            amountNQT = nqt.toString(),
                            recipient = address,
                            feeNQT = feeNQT)

                    request.subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe ({ _ ->
                                progress_bar_holder_send.visibility = View.INVISIBLE
                                this@SendActivity.setResult(RESULT_SENT)
                                this@SendActivity.finish()
                            }, { error -> Log.d("SEND ERROR", error.message) })

                }, { error -> Log.d("SEND ERROR", error.message) })

    }



    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when (requestCode) {
            ADDRESS_SCAN_REQUEST -> {
                if (data != null) {
                    val address = data.getStringExtra(ScanActivity.QR_DATA_STRING)
                    Log.d("SCAN", address)
                    editTextAddress.setText(address)
                }
            }
        }
    }

    fun backToMainActivity(){
        val upIntent = NavUtils.getParentActivityIntent(this)
        if (NavUtils.shouldUpRecreateTask(this, upIntent!!)) {
            TaskStackBuilder.create(this)
                    .addNextIntentWithParentStack(upIntent)
                    .startActivities()
        } else {
            NavUtils.navigateUpTo(this, upIntent)
        }
    }

}
