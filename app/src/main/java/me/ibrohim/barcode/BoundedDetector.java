package me.ibrohim.barcode;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.ImageFormat;
import android.graphics.Rect;
import android.graphics.YuvImage;
import android.util.SparseArray;

import com.google.android.gms.vision.Detector;
import com.google.android.gms.vision.Frame;

import java.io.ByteArrayOutputStream;

/**
 * @author ibrohimislam
 */

class BoundedDetector<T> extends Detector<T>{
    private Detector mDelegate;

    private boolean isInitialized = false;
    private int width;
    private int height;
    private int right;
    private int left;
    private int bottom;
    private int top;

    BoundedDetector(Detector delegate) {
        mDelegate = delegate;
    }

    public void setBoundSize(int width, int height, int boundSize) {
        this.width = width;
        this.height = height;

        this.right = (width / 2) + (boundSize / 2);
        this.left = (width / 2) - (boundSize / 2);
        this.bottom = (height / 2) + (boundSize / 2);
        this.top = (height / 2) - (boundSize / 2);

        isInitialized = true;
    }

    private void selfAdaptBound(Frame frame) {
        if (!isInitialized) {
            this.width = frame.getMetadata().getWidth();
            this.height = frame.getMetadata().getHeight();

            int minBound = Math.min(width, height);

            this.right = (width / 2) + (minBound / 2);
            this.left = (width / 2) - (minBound / 2);
            this.bottom = (height / 2) + (minBound / 2);
            this.top = (height / 2) - (minBound / 2);

            isInitialized = true;
        }
    }

    public SparseArray detect(Frame frame) {
        selfAdaptBound(frame);

        YuvImage yuvImage = new YuvImage(frame.getGrayscaleImageData().array(), ImageFormat.NV21, width, height, null);
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        yuvImage.compressToJpeg(new Rect(left, top, right, bottom), 100, byteArrayOutputStream);
        byte[] jpegArray = byteArrayOutputStream.toByteArray();
        Bitmap bitmap = BitmapFactory.decodeByteArray(jpegArray, 0, jpegArray.length);

        Frame croppedFrame =
                new Frame.Builder()
                        .setBitmap(bitmap)
                        .setRotation(frame.getMetadata().getRotation())
                        .build();

        return mDelegate.detect(croppedFrame);
    }

    public boolean isOperational() {
        return mDelegate.isOperational();
    }

    public boolean setFocus(int id) {
        return mDelegate.setFocus(id);
    }
}
